-- CreateTable
CREATE TABLE "device_data" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "serviceUUID" TEXT NOT NULL,
    "timestamp" TEXT NOT NULL,
    "rss" REAL NOT NULL,
    "distance" REAL NOT NULL
);
